# Codeception PHAR

A phar bundle with some useful codeception modules.

## Download

You can see the latest PHAR files on the [releases page](https://gitlab.com/koehnlein/codeception-phar/-/releases).
